package face

import (
	"fmt"
	"time"

	"github.com/ajstarks/openvg"
)

type Face struct {
	x, y          int
	width, height int

	hour, minute, second int
}

func New(x, y int) *Face {
	return &Face{
		x: x,
		y: y,
	}
}

func (f *Face) Position() (x, y int) {
	return f.x, f.y
}

func (f *Face) Size() (w, h int) {
	return f.width, f.height
}

func (f *Face) Update(tick int) {
	now := time.Now()
	f.hour = now.Hour() % 12
	f.minute = now.Minute()
	f.second = now.Second()
}

func (f *Face) Draw() {
	// Draw the time

	//openvg.FillRGB(0, 213, 255, 1)
	//openvg.StrokeRGB(0, 213, 255, 1)
	//openvg.FillRGB(0, 213, 255, 1)
	openvg.FillRGB(0, 0, 0, 1)

	hm := fmt.Sprintf("%02d:%02d", f.hour, f.minute)
	s := fmt.Sprintf(":%02d", f.second)

	face := "mono"

	hmWidth := openvg.TextWidth(hm, face, 80)
	openvg.Text(0, 0, hm, face, 80)
	openvg.Text(hmWidth, 0, s, face, 40)
}
