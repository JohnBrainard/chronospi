package clockui

type Component interface{
	// Position returns the X/Y coordinates of the component.
	Position() (x int, y int)

	// Size returns the Width/Height dimensions of the component.
	Size() (w int, h int)

	// Update is called before drawing
	Update(tick int)

	// Draw
	Draw()
}
