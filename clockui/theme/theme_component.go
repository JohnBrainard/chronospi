package theme

import (
	"fmt"
	"time"

	"bitbucket.org/JohnBrainard/consoleclock/theme"
	"github.com/ajstarks/openvg"
)

type ThemeComponent struct {
	theme *theme.Theme

	hour, minute, second int
	date                 string
}

func New(t *theme.Theme) *ThemeComponent {
	component := ThemeComponent{theme: t}
	return &component
}

func (c *ThemeComponent) Position() (x, y int) {
	return 0, 0
}

func (c *ThemeComponent) Size() (w, h int) {
	return 800, 480
}

func (c *ThemeComponent) Update(tick int) {
	now := time.Now()
	c.hour, c.minute, c.second = now.Clock()

	if c.hour%12 == 0 {
		c.hour = 12
	} else {
		c.hour = c.hour % 12
	}

	c.date = now.Format("Monday, January 2, 2006")
}

func (c *ThemeComponent) Draw() {
	c.drawBackground(c.theme.Background)
	c.drawFrame(c.theme.Frame.Color, c.theme.Frame.Pos, c.theme.Frame.Size)
	c.drawDate()
	c.drawTime()
}

func (c *ThemeComponent) drawBackground(color theme.Color) {
	openvg.Background(color.Red, color.Green, color.Blue)
}

func (c *ThemeComponent) drawFrame(color theme.Color, pos theme.Position, size theme.Size) {
	openvg.StrokeWidth(4.0)
	openvg.StrokeRGB(color.Red, color.Green, color.Blue, color.Alpha.VGFloat())

	openvg.RoundrectOutline(
		openvg.VGfloat(pos.X),
		openvg.VGfloat(pos.Y),
		openvg.VGfloat(size.W),
		openvg.VGfloat(size.H),
		openvg.VGfloat(10.0),
		openvg.VGfloat(10.0))
}

func (c *ThemeComponent) drawDate() {
	color := c.theme.Date.Color
	pos := c.theme.Date.Pos
	font := c.theme.Date.Font

	openvg.FillRGB(color.Red, color.Green, color.Blue, color.Alpha.VGFloat())
	openvg.Text(
		openvg.VGfloat(pos.X),
		openvg.VGfloat(pos.Y),
		c.date,
		font.TTF,
		int(font.Points))
}

func (c *ThemeComponent) drawTime() {
	color := c.theme.Face.Color
	pos := c.theme.Face.Pos
	font := c.theme.Face.Font

	hour := fmt.Sprintf("%02d", c.hour)
	minute := fmt.Sprintf("%02d", c.minute)

	hourWidth := openvg.TextWidth(hour, font.TTF, int(font.Points))
	colonWidth := openvg.TextWidth(":", font.TTF, int(font.Points))

	openvg.FillRGB(color.Red, color.Green, color.Blue, color.Alpha.VGFloat())
	openvg.Text(
		openvg.VGfloat(pos.X),
		openvg.VGfloat(pos.Y),
		hour,
		font.TTF,
		int(font.Points))

	if c.second%2 == 0 {
		openvg.Text(
			openvg.VGfloat(pos.X)+hourWidth,
			openvg.VGfloat(pos.Y),
			":",
			font.TTF,
			int(font.Points))
	}

	openvg.Text(
		openvg.VGfloat(pos.X)+hourWidth+colonWidth,
		openvg.VGfloat(pos.Y),
		minute,
		font.TTF,
		int(font.Points))
}
