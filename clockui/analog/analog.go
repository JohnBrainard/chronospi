package analog

import (
	"time"

	"github.com/ajstarks/openvg"

	"bitbucket.org/JohnBrainard/consoleclock/clockui"
)

type Analog struct {
	width, height int
	x, y          int
	radius        int
	clock         *clockui.ClockUI

	hour, minute, second int
}

func New(clock *clockui.ClockUI, x, y, width, height int) *Analog {
	return &Analog{
		clock:  clock,
		width:  width,
		height: height,
		x:      x,
		y:      y,
		radius: clockui.MinInt(width, height),
	}
}

func (a *Analog) Position() (x, y int) {
	return a.x, a.y
}

func (a *Analog) Size() (w, h int) {
	return a.width, a.height
}

func (a *Analog) Update(tick int) {
	now := time.Now()
	a.hour = now.Hour()
	a.minute = now.Minute()
	a.second = now.Second()
}

func (a *Analog) Draw() {
	//a.drawFace()
}

func (a *Analog) drawFace() {
	cx, cy := openvg.VGfloat(a.width/2), openvg.VGfloat(a.height/2)
	openvg.StrokeWidth(openvg.VGfloat(3.0))
	openvg.StrokeColor("black")
	openvg.CircleOutline(cx, cy, openvg.VGfloat(a.radius))
}
