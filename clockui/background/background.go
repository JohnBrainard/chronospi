package background

import "github.com/ajstarks/openvg"

type Background struct {
	r, g, b uint8
}

func New(r, g, b uint8) *Background {
	return &Background{
		r: r,
		g: g,
		b: b,
	}
}

func (bg *Background) Position() (x, y int) {
	return 0, 0
}

func (bg *Background) Size() (w, h int) {
	return 0, 0
}

func (bg *Background) Update(frame int) {}
func (bg *Background) Draw() {
	openvg.Background(bg.r, bg.g, bg.b)
}
