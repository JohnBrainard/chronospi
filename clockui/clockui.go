package clockui

import (
	"context"
	"log"
	"time"

	"github.com/ajstarks/openvg"

	"golang.org/x/time/rate"
)

type ClockUI struct {
	context context.Context
	limiter *rate.Limiter

	frame uint64

	width, height int

	components []Component
}

func New(context context.Context) *ClockUI {
	ui := &ClockUI{
		context: context,
		frame:   0,
	}

	ui.initFrameChannel(1)

	return ui
}

func (ui *ClockUI) Size() (width, height int) {
	return ui.width, ui.height
}

func (ui *ClockUI) AddComponent(c Component) {
	ui.components = append(ui.components, c)
}

func (ui *ClockUI) Start() {
	ui.width, ui.height = openvg.Init()
	openvg.BackgroundColor("black")

	for {
		ui.updateComponents()
		ui.renderComponents()

		err := ui.limiter.Wait(ui.context)
		if err != nil {
			log.Fatal("Failed next frame", err)
		}
	}
}

func (ui *ClockUI) updateComponents() {
	for _, c := range ui.components {
		c.Update(0)
	}
}

func (ui *ClockUI) renderComponents() {
	openvg.Start(ui.width, ui.height)
	openvg.WindowClear()
	for _, c := range ui.components {
		x, y := c.Position()
		openvg.Translate(openvg.VGfloat(x), openvg.VGfloat(y))
		c.Draw()
	}

	openvg.End()
}

func (ui *ClockUI) initFrameChannel(fps int) {
	limit := rate.Every(time.Second / time.Duration(fps))
	ui.limiter = rate.NewLimiter(limit, 1)
}
