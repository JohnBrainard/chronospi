package calendar

import "time"

type Calendar struct {
	year, month, day int
	x, y             int
}

func New() *Calendar {
	return &Calendar{}
}

func (c *Calendar) Position() (x, y int) {
	return c.x, c.y
}

func (c *Calendar) Size() (w, h int) {
	return 100, 100
}

func (c *Calendar) Update(tick int) {
	year, month, day := time.Now().Date()
	c.day = day
	c.month = int(month)
	c.year = year
}

func (c *Calendar) Draw() {

}
