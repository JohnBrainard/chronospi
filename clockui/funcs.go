package clockui

func MinInt(a, b int) int {
	if b < a {
		return b
	}
	return a
}

func MaxInt(a, b int) int {
	if b > a {
		return b
	}
	return a
}
