package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"bitbucket.org/JohnBrainard/consoleclock/clockui"
	themecomponent "bitbucket.org/JohnBrainard/consoleclock/clockui/theme"
	"bitbucket.org/JohnBrainard/consoleclock/theme"
)

const usage = `Usage:
%s <path-to-theme>

  <path-to-theme>: directory containing theme JSON and assets
`

func main() {
	if len(os.Args) != 2 {
		fmt.Printf(usage, os.Args[0])
		os.Exit(1)
	}

	ctx := context.Background()
	clock := clockui.New(ctx)

	clockTheme, err := theme.LoadTheme(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Using theme: %v", clockTheme)

	clock.AddComponent(themecomponent.New(clockTheme))

	//clock.AddComponent(background.New(255, 255, 255))
	//clock.AddComponent(face.New(0, 2))
	//clock.AddComponent(analog.New(clock, 275, 125, 250, 250))
	clock.Start()
}
