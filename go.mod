module bitbucket.org/JohnBrainard/ChronosPi

go 1.13

require (
	github.com/ajstarks/openvg v0.0.0-20191008131700-c6885d824eb8
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0
)
