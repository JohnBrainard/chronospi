package theme

import "fmt"

type Font struct {
	TTF    string  `json:"ttf"`
	Points float64 `json:"points"`
}

func (f *Font) String() string {
	return fmt.Sprintf("Font{ttf=%s, points=%f}", f.TTF, f.Points)
}
