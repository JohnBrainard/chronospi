package theme

import (
	"fmt"

	"github.com/ajstarks/openvg"
)

type AlphaVal float64

type Color struct {
	Red   uint8    `json:"r"`
	Green uint8    `json:"g"`
	Blue  uint8    `json:"b"`
	Alpha AlphaVal `json:"a"`
}

func (v AlphaVal) VGFloat() openvg.VGfloat {
	return openvg.VGfloat(v)
}

func (c Color) String() string {
	return fmt.Sprintf("Color{r:%03d, g:%03d, b:%03d, a:%0.2f}", c.Red, c.Green, c.Blue, float64(c.Alpha))
}
