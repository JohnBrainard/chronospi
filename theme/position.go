package theme

import "fmt"

type Position struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

func (p *Position) String() string {
	return fmt.Sprintf("Position{x=%f, y=%f", p.X, p.Y)
}
