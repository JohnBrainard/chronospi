package theme

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type Theme struct {
	Background Color `json:"background"`
	Face       struct {
		Font  Font     `json:"font"`
		Pos   Position `json:"pos"`
		Color Color    `json:"color"`
	} `json:"face"`

	Date struct {
		Font  Font     `json:"font"`
		Pos   Position `json:"pos"`
		Color Color    `json:"color"`
	} `json:"date"`

	Frame struct {
		Pos   Position `json:"pos"`
		Color Color    `json:"color"`
		Size  Size     `json:"size"`
	}
}

func LoadTheme(path string) (*Theme, error) {
	var theme Theme
	var err error

	reader, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err = reader.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	if err = json.NewDecoder(reader).Decode(&theme); err != nil {
		return nil, err
	}

	return &theme, nil
}

const stringFormat = `Theme:{
  Background=%v,
  Face{
    Font: %v,
    Pos: %v,
    Color: %v
  }
  Date{
    Font: %v,
    Pos: %v,
    Color: %v
  }
  Frame{
    Pos: %v,
    Size: %v,
    Color: %v
  }
}`

func (t *Theme) String() string {
	return fmt.Sprintf(stringFormat,
		t.Background,
		t.Face.Font,
		t.Face.Pos,
		t.Face.Color,
		t.Date.Font,
		t.Date.Pos,
		t.Date.Color,
		t.Frame.Pos,
		t.Frame.Size,
		t.Frame.Color)
}
