package theme

import "fmt"

type Size struct {
	W float64 `json:"w"`
	H float64 `json:"h"`
}

func (s *Size) String() string {
	return fmt.Sprintf("Size{w=%f, h=%f}", s.W, s.H)
}
